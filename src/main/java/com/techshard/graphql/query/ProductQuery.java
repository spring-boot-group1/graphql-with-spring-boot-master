package com.techshard.graphql.query;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.techshard.graphql.dao.entity.Product;
import com.techshard.graphql.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ProductQuery implements GraphQLQueryResolver {

    @Autowired
    private ProductService productService;

    public List<Product> getProducts(final int count) {
        return this.productService.getAllProducts(count);
    }

    public Optional<Product> getProduct(final int id) {
        return this.productService.getProduct(id);
    }
}
