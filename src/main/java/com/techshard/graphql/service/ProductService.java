package com.techshard.graphql.service;

import com.techshard.graphql.dao.entity.Product;
import com.techshard.graphql.dao.repository.ProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductService {

    private final ProductRepository productRepository ;

    public ProductService(final ProductRepository vehicleRepository, ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Transactional(readOnly = true)
    public List<Product> getAllProducts(final int count) {
        return this.productRepository.findAll().stream().limit(count).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public Optional<Product> getProduct(final int id) {
        return this.productRepository.findById(id);
    }

    @Transactional
    public Product createProduct(String name, String description, String category, Integer price, String date) {

        final Product product = new Product();
        product.setName(name);
        product.setCategory(category);
        product.setDate(LocalDate.parse(date));
        product.setDescription(description);
        product.setPrice(price);
        return this.productRepository.save(product);
    }
}
